function listOfElements(arrayElements, parent) {
    let ul = document.createElement('ul');
    arrayElements.forEach(function (element) {
    
        let li = document.createElement('li');
        li.textContent = element;
        ul.appendChild(li);
        
    });

    let par = parent || document.body;
   par.appendChild(ul);

   
}

   let myArr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
   let myArr2 = ["1", "2", "3", "sea", "user", 23];

   listOfElements(myArr1);
   listOfElements(myArr2);


// 1.Можна створити новий тег під назвою <custom-tag> та додати його, цей підхід дає вам можливість створювати свої
// власні HTML елементи, але такий підхід може не взаємодіяти з CSS чи JavaScript так само, як вбудовані теги.
// 2.insertAdjacentHTML використовується для вставки HTML-рядка або фрагмента HTML-коду біля визначеного елемента.
// Перший параметр цього методу визначає місце вставки, існуючий контент визначеного елемента буде зміщено відповідно(beforebegin,afterbegin і т.д)
// 3.Щоб видалити елемент зі сторінки можна скористатися методом remove(),цей метод видаляє викликаємий об'єкт або елемент з DOM.